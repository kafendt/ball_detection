import onnx


# Load the ONNX model
def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(
        description="This programs name", formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("input_path", help="help_text")
    args = arg_parser.parse_args()
    return args.input_path


model = onnx.load(parse_args())

# Check that the IR is well formed
print(onnx.checker.check_model(model))

# Print a human readable representation of the graph
print(onnx.helper.printable_graph(model.graph))
