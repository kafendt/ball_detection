#include <iostream>
#include <cstdio>
#include <opencv4/opencv2/dnn.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/imgproc.hpp>

int main(int argc, const char *argv[]){
  using namespace cv;
  using namespace dnn;
  Mat image, blob;

  // Read the network
  std::string pb_file = std::string(argv[1]);
  std::string pbtxt_file = pb_file.substr(0, pb_file.size()-3) + ".pbtxt";
  Net network = readNetFromTensorflow(pb_file);

  // Read an image
  image = imread(argv[2], IMREAD_UNCHANGED);

  // Resize the image
  // Size size(15, 15);//the dst image size,e.g.100x100
  // resize(image, image, size, INTER_LINEAR);//resize image
  image.convertTo(image, CV_32FC3, 1.0 / 255.0, 0);
  // normalize(image, image, 0.0, 1.0, NORM_MINMAX, CV_32FC3);

  //! [Create a 4D blob from a frame]
  blob = blobFromImage(image, 1.0, Size(), Scalar(), true);

  //! [Set input blob]
  network.setInput(blob);
  //! [Make forward pass]
  Mat outputs = network.forward();

  //! [Get a class with a highest score]
  Point classIdPoint;
  double confidence;
  // std::cout << outputs << std::endl;
  minMaxLoc(outputs.reshape(1, 1), 0, &confidence, 0, &classIdPoint);
  int classId = classIdPoint.x;
  std::cout << classId << std::endl;
  std::cout << outputs << std::endl;

  return classId;
}
