#!/usr/bin/env python

import os

path = os.path.dirname(os.path.realpath(__file__))
build_path = "{}/build".format(path)

if not os.path.isdir(build_path):
    os.makedirs(build_path)

os.chdir(build_path)
os.system("cmake ..")
os.system("make -j9")
os.rename("{}".format(path+"/build/compile_commands.json"),"{}".format(path+"/compile_commands.json"))