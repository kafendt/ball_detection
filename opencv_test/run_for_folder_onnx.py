import os
import torch.onnx

def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(
        description="This programs name", formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("net", help="help_text")
    arg_parser.add_argument("input_path", help="help_text")
    arg_parser.add_argument("label", help="help_text")
    args = arg_parser.parse_args()
    return args.net, args.input_path, args.label


if __name__ == '__main__':
    net, input_path, label = parse_args()
    label = int(label)

    from subprocess import Popen, PIPE
    from tqdm import tqdm
    predictions = []
    image_paths = os.listdir(input_path)
    pbar = tqdm(total = len(image_paths))
    for image in image_paths:
        image = os.path.join(input_path, image)

        pbar.update(1)
        amount_correct = predictions.count(label)
        acc = amount_correct / len(predictions)
        pbar.set_description(f"Acc {acc}")

    amount_correct = predictions.count(label)
    acc = amount_correct / len(predictions)
    print("Accuracy: {}".format(acc))
