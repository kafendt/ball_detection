import os
from PIL import Image
from random import randint
from tqdm import tqdm
from collections import defaultdict


def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(
        description="This programs name", formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("input_dir", help="help_text")
    arg_parser.add_argument("output_dir", help="help_text")
    arg_parser.add_argument("val_size", type=int, nargs='?', default=30, help="val size in %")
    arg_parser.add_argument("test_size", type=int, nargs='?', default=0, help="test size in %")
    args = arg_parser.parse_args()
    return args.input_dir, args.output_dir, args.val_size, args.test_size


def collect_images(input_dir: str):
    images_dict = defaultdict(list)
    for path, directories, files in os.walk(input_dir):
        wanted_files = [file for file in files if ".png" in file]
        for file in sorted(wanted_files):
            images_dict[path].append(os.path.join(path, file))

    return images_dict


def create_split(images_dict: dict, output_dir: str, val_size: int, test_size: int):
    for path, images in images_dict.items():
        # Retrieve the class name from the dict
        class_name = os.path.basename(path)

        # Create the needed folders
        train_size = 100 - val_size - test_size
        if train_size > 0:
            train_dir = os.path.join(output_dir, "train", class_name)
            os.makedirs(train_dir, exist_ok=True)
        if val_size > 0:
            val_dir = os.path.join(output_dir, "val", class_name)
            os.makedirs(val_dir, exist_ok=True)
        if test_size > 0:
            test_dir = os.path.join(output_dir, "test", class_name)
            os.makedirs(test_dir, exist_ok=True)

        train_cutoff = train_size
        val_cutoff = train_cutoff + val_size
        for image_path in tqdm(images):
            rand_num = randint(1, 100)
            if rand_num <= train_cutoff:
                output_path = os.path.join(train_dir, os.path.basename(image_path))
                img = Image.open(image_path)
                img = img.resize((15, 15))
                img.save(output_path)
            elif rand_num <= val_cutoff:
                output_path = os.path.join(val_dir, os.path.basename(image_path))
                img = Image.open(image_path)
                img = img.resize((15, 15))
                img.save(output_path)
            else:
                output_path = os.path.join(test_dir, os.path.basename(image_path))
                img = Image.open(image_path)
                img = img.resize((15, 15))
                img.save(output_path)


if __name__ == '__main__':
    # Read command line arguments
    input_dir, output_dir, val_size, test_size = parse_args()

    # Collect all images in a per class dict
    images_dict = collect_images(input_dir)

    # Create the split
    create_split(images_dict, output_dir, val_size, test_size)
