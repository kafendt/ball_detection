import torch
from torch import nn
import torch.optim as optim
from torchvision import transforms
import torch.nn.functional as F

from pytorch_tools.data import prepare_loader
from pytorch_tools.train_ball import train

# Define transformations
image_input_size = (15, 15)

train_transformations = transforms.Compose([
    transforms.Resize(image_input_size),
    transforms.RandomAffine(0, (0.1, 0.1)),
    transforms.ColorJitter(0.3, 0.3, 0.3),
    transforms.RandomVerticalFlip(),
    transforms.RandomRotation(15),
    transforms.ToTensor(),
])
valid_transformations = transforms.Compose([
    transforms.Resize(image_input_size),
    transforms.ToTensor(),
])

# prepare loaders
data_path = "/home/chris/data/hulks/classification/labeled_manually"
train_loader, valid_loader = prepare_loader(
    data_path,
    train_transformations,
    valid_transformations,
)

class CurrentBallNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=5, kernel_size=(2, 2))
        self.batch1 = nn.BatchNorm2d(5)
        # relu1

        self.fc1 = nn.Linear(14 * 14 * 5, 16)
        # relu2
        self.fc2 = nn.Linear(16, 19)
        # relu3
        self.fc3 = nn.Linear(19, 2)
        # relu4
        self.fc4 = nn.Linear(2, 20)
        # relu5
        self.logits = nn.Linear(20, 2)

    def forward(self, x):
        x = self.conv1(x)
        x = self.batch1(x)
        x = F.relu(x)
        x = x.view(-1, self.num_flat_features(x))
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        x = F.relu(x)
        x = self.fc3(x)
        x = F.relu(x)
        x = self.fc4(x)
        x = F.relu(x)
        x = self.logits(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features

current_ball_net = CurrentBallNet()

# Define loss
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(current_ball_net.parameters(), lr=0.01, momentum=0.9)

# Run training
current_ball_net, best_train_acc, best_val_acc = train(current_ball_net, criterion, optimizer, train_loader, valid_loader, 50)
