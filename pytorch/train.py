from torch import nn
import torch.optim as optim
from torchvision import transforms
import torch.nn.functional as F

from pytorch_tools.data import prepare_loader
from pytorch_tools.train import train


def define_network():
    class Net(nn.Module):
        def __init__(self):
            super(Net, self).__init__()
            # 1 input image channel, 6 output channels, 5x5 square convolution
            # kernel
            self.conv1 = nn.Conv2d(3, 20, 3)
            self.batch1 = nn.BatchNorm2d(20)
            self.conv2 = nn.Conv2d(20, 10, 3)
            self.batch2 = nn.BatchNorm2d(10)
            # an affine operation: y = Wx + b

            self.fc1 = nn.Linear(96 * 96 * 10, 120)
            self.fc2 = nn.Linear(120, 84)
            self.fc3 = nn.Linear(84, 2)

        def forward(self, x):
            x = self.conv1(x)
            x = self.batch1(x)
            x = F.relu(x)
            x = self.conv2(x)
            x = self.batch2(x)
            x = F.relu(x)
            x = F.relu(x)
            x = x.view(-1, self.num_flat_features(x))
            x = self.fc1(x)
            x = F.relu(x)
            x = self.fc2(x)
            x = F.relu(x)
            x = self.fc3(x)
            return x

        def num_flat_features(self, x):
            size = x.size()[1:]  # all dimensions except the batch dimension
            num_features = 1
            for s in size:
                num_features *= s
            return num_features

    return Net()


if __name__ == '__main__':
    # Define transformations
    image_input_size = (100, 100)
    train_transformations = transforms.Compose([
        transforms.Resize(image_input_size),
        transforms.RandomAffine(0, (0.1, 0.1)),
        transforms.RandomVerticalFlip(),
        transforms.RandomRotation(15),
        transforms.ToTensor(),
    ])
    valid_transformations = transforms.Compose([
        transforms.Resize(image_input_size),
        transforms.ToTensor(),
    ])

    # prepare loaders
    data_path = "/home/chris/data/hulks/classification/labeled_manually"
    print("Preparing data loaders...")
    train_loader, valid_loader = prepare_loader(
        data_path,
        train_transformations,
        valid_transformations,
    )

    net = define_network()

    # Define loss
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.01, momentum=0.9)

    # Run the training
    net, best_train_acc, best_val_acc = train(net, criterion, optimizer, train_loader, valid_loader, 3)
