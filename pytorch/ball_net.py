import torch
import numpy as np
from torch import nn
import torch.optim as optim
from torchvision import transforms
import torch.nn.functional as F

from pytorch.data import prepare_loader
from pytorch.train_ball import train


def to_tensor(pic):
    """Convert a ``PIL Image`` or ``numpy.ndarray`` to tensor.
    See ``ToTensor`` for more details.
    Args:
        pic (PIL Image or numpy.ndarray): Image to be converted to tensor.
    Returns:
        Tensor: Converted image.
    """

    # handle PIL Image
    if pic.mode == 'I':
        img = torch.from_numpy(np.array(pic, np.int32, copy=False))
    elif pic.mode == 'I;16':
        img = torch.from_numpy(np.array(pic, np.int16, copy=False))
    elif pic.mode == 'F':
        img = torch.from_numpy(np.array(pic, np.float32, copy=False))
    elif pic.mode == '1':
        img = 255 * torch.from_numpy(np.array(pic, np.uint8, copy=False))
    else:
        img = torch.ByteTensor(torch.ByteStorage.from_buffer(pic.tobytes()))
    # PIL image mode: L, LA, P, I, F, RGB, YCbCr, RGBA, CMYK
    if pic.mode == 'YCbCr':
        nchannel = 3
    elif pic.mode == 'I;16':
        nchannel = 1
    else:
        nchannel = len(pic.mode)
    img = img.view(pic.size[1], pic.size[0], nchannel)
    # put it from HWC to CHW format
    # yikes, this transpose takes 80% of the loading time/CPU
    img = img.transpose(0, 1).transpose(0, 2).contiguous()
    if isinstance(img, torch.ByteTensor):
        return img.float()
    else:
        return img


class ToTensor(object):
    def __call__(self, pic):
        """
        Args:
            pic (PIL Image or numpy.ndarray): Image to be converted to tensor.
        Returns:
            Tensor: Converted image.
        """
        return to_tensor(pic)

    def __repr__(self):
        return self.__class__.__name__ + '()'


class FCNetVar(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=5, kernel_size=(3, 3))
        self.batch1 = nn.BatchNorm2d(5)
        # relu1
        self.conv2 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=(3, 3))
        self.batch2 = nn.BatchNorm2d(5)
        # relu2
        self.conv3 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=(3, 3))
        self.batch3 = nn.BatchNorm2d(5)
        # relu3
        self.conv4 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=(3, 3))
        self.batch4 = nn.BatchNorm2d(5)
        # relu4
        self.conv5 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=(3, 3))
        self.batch5 = nn.BatchNorm2d(5)
        # relu5
        self.conv6 = nn.Conv2d(in_channels=5, out_channels=2, kernel_size=(3, 3))
        self.logits = nn.AdaptiveAvgPool2d(1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.batch1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = self.batch2(x)
        x = F.relu(x)
        x = self.conv3(x)
        x = self.batch3(x)
        x = F.relu(x)
        x = self.conv4(x)
        x = self.batch4(x)
        x = F.relu(x)
        x = self.conv5(x)
        x = self.batch5(x)
        x = F.relu(x)
        x = self.conv6(x)
        x = F.relu(x)
        x = self.logits(x)
        x = x.view(x.size(0), -1)
        return x


if __name__ == '__main__':
    # Define transformations
    image_input_size = (30, 30)

    train_transformations = transforms.Compose([
        transforms.Resize(image_input_size),
        transforms.RandomAffine(0, (0.1, 0.1)),
        transforms.ColorJitter(0.3, 0.3, 0.3),
        transforms.RandomVerticalFlip(),
        transforms.RandomRotation(15),
        ToTensor(),
    ])
    valid_transformations = transforms.Compose([
        transforms.Resize(image_input_size),
        transforms.ToTensor(),
    ])

    # prepare loaders
    data_path = "/home/chris/data/hulks/classification/labeled_manually"
    train_loader, valid_loader = prepare_loader(
        data_path,
        train_transformations,
        valid_transformations,
    )

    fcnet_var = FCNetVar()

    # Define loss
    weights = torch.tensor([2.0, 1.0]).cuda()
    criterion = nn.CrossEntropyLoss(weight=weights)
    optimizer = optim.SGD(fcnet_var.parameters(), lr=0.01, momentum=0.9)

    fcnet_var, best_train_acc, best_current_ball_net = train(
        fcnet_var,
        criterion,
        optimizer,
        train_loader,
        valid_loader,
        100,
    )
