from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, BatchNormalization
from keras.layers import Activation, Flatten, Dense, Reshape
from keras.callbacks import Callback
import keras.backend as K
import tensorflow as tf
import json
import yaml
import os


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(
            set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = tf.graph_util.convert_variables_to_constants(session, input_graph_def,
                                                                    output_names, freeze_var_names)
        return frozen_graph


def fpr(y_true, y_pred):
    # Calculate how many background images were predicted false
    pred_class = tf.argmax(y_pred, 1)
    label = tf.argmax(y_true, 1)
    zeros = tf.zeros_like(label)
    ones = tf.ones_like(label)

    fp_op = tf.reduce_sum(
        tf.cast(
            tf.logical_and(
                tf.equal(label, zeros),
                tf.equal(pred_class, ones),
            ),
            "float",
        ),
    )
    amount_backgrounds = tf.reduce_sum(tf.cast(tf.equal(label, zeros), tf.int8))

    def calc_fpr():
        return fp_op / tf.cast(amount_backgrounds, tf.float32)

    fpr_op = tf.cond(tf.equal(amount_backgrounds, 0), lambda: 0.0, calc_fpr)

    K.get_session().run(tf.local_variables_initializer())
    return fpr_op


def tpr(y_true, y_pred):
    # Ball class is 1, ergo to find false positives need to find
    # all cases where 1 is predicted but label is 0

    # Calculate how many background images were predicted false
    pred_class = tf.argmax(y_pred, 1)
    label = tf.argmax(y_true, 1)
    zeros = tf.zeros_like(label)
    ones = tf.ones_like(label)

    tp_op = tf.reduce_sum(
        tf.cast(
            tf.logical_and(
                tf.equal(label, ones),
                tf.equal(pred_class, ones),
            ),
            "float",
        ),
    )
    amount_balls = tf.reduce_sum(tf.cast(tf.equal(label, ones), tf.int8))

    def calc_tpr():
        return tp_op / tf.cast(amount_balls, tf.float32)

    tpr_op = tf.cond(tf.equal(amount_balls, 0), lambda: 0.0, calc_tpr)

    K.get_session().run(tf.local_variables_initializer())
    return tpr_op


if __name__ == '__main__':
    model = Sequential()
    model.add(Conv2D(5, (3, 3), input_shape=(15, 15, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Reshape((13 * 13 * 5, )))  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(16))
    model.add(Activation('relu'))
    model.add(Dense(19))
    model.add(Activation('relu'))
    model.add(Dense(2))
    model.add(Activation('relu'))
    model.add(Dense(20))
    model.add(Activation('relu'))
    model.add(Dense(2))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy', fpr, tpr])

    batch_size = 32

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=30,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
    )

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    # this is a generator that will read pictures found in
    # subfolers of 'data/train', and indefinitely generate
    # batches of augmented image data
    train_generator = train_datagen.flow_from_directory(
        '/data/train',  # this is the target directory
        target_size=(15, 15),  # all images will be resized to 15x15
        batch_size=batch_size,
        class_mode='categorical')

    # this is a similar generator, for validation data
    validation_generator = test_datagen.flow_from_directory(
        '/data/val',
        target_size=(15, 15),
        batch_size=batch_size,
        class_mode='categorical',
    )

    class SaveModel(Callback):
        def on_epoch_end(self, epoch, logs=None):
            frozen_graph = freeze_session(
                K.get_session(),
                output_names=[out.op.name for out in self.model.outputs],
            )
            tf.train.write_graph(
                frozen_graph,
                os.path.dirname(os.path.realpath(__file__)),
                "model_epoch_{}.pb".format(epoch),
                as_text=False,
            )
            tf.train.write_graph(
                frozen_graph,
                os.path.dirname(os.path.realpath(__file__)),
                "model_epoch_{}.pbtxt".format(epoch),
            )

    class SaveLog(Callback):
        def on_epoch_end(self, epoch, logs=None):
            # Save metrics and weights
            logfile = os.path.dirname(os.path.realpath(__file__)) + "/metrics_log.json"
            if epoch <= 0:
                with open(logfile, 'w') as file:
                    json.dump({'epoch_{}'.format(epoch): logs}, file, indent=4)
            else:
                with open(logfile, 'r') as file:
                    content = json.load(file)

                with open(logfile, 'w') as file:
                    content.update({'epoch_{}'.format(epoch): logs})
                    json.dump(content, file, indent=4)

            # Save weights
            logfile = os.path.dirname(os.path.realpath(__file__)) + "/weights_epoch_{}.json".format(epoch)
            weight_dict = dict()
            for layer in self.model.layers:
                weights = layer.get_weights()
                if len(weights) > 0:
                    if len(weights) == 1:
                        weight_dict[layer.name] = weights[0].tolist()
                    elif len(weights) == 2:
                        weight_dict[layer.name] = {
                            'weights': weights[0].tolist(),
                            'biases': weights[1].tolist(),
                        }
                    else:
                        print("UNKNOWN WEIGHT LENGTH! Layer: {}".format(layer.name))

            with open(logfile, 'w') as file:
                json.dump(weight_dict, file, indent=4)

    model.fit_generator(
        train_generator,
        steps_per_epoch=63982 // batch_size,
        epochs=20,
        validation_data=validation_generator,
        validation_steps=7044 // batch_size,
        callbacks=[SaveModel(), SaveLog()],
        use_multiprocessing=True,
        workers=8,
    )
