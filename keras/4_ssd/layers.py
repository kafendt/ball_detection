import tensorflow as tf
from typing import Tuple, Optional


class Conv_bn_act(tf.keras.layers.Layer):
    def __init__(
            self,
            filters: int,
            kernel_size: Tuple[int, int],
            strides: int = 1,
            padding: str = "valid",
            activation: Optional[str] = None,
            dilation_rate: Tuple[int, int] = (1, 1),
            trainable: bool = True,
    ):
        super().__init__()
        self.conv = tf.keras.layers.Conv2D(
            filters=filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=padding,
            dilation_rate=dilation_rate,
            activation=activation,
            trainable=trainable,
        )
        self.bn = tf.keras.layers.BatchNormalization(trainable=trainable)
        self.act = tf.keras.layers.ReLU()

    def call(self, inputs: tf.Tensor) -> tf.Tensor:
        conv = self.conv(inputs)
        bn = self.bn(conv)
        act = self.act(bn)
        return act
