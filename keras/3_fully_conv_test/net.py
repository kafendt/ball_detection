from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, BatchNormalization
from keras.layers import Activation, Lambda, Dense, Reshape
from keras.callbacks import Callback
import keras.backend as K
import tensorflow as tf
from tensorflow import NodeDef
import json
import os

import logging

logger = logging.getLogger("__name__")
logger.setLevel(logging.INFO)
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
formatter = logging.Formatter('[%(levelname)s]%(name)s: %(message)s')
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


def node_name(name: str):
    return name.split(':')[0]


def fix_batch_norm(graph):
    nodes = dict()
    for node in graph.node:
        nodes[node.name] = node
    fix_counter = 0
    result_nodes = []
    for node in graph.node:
        if node.op == "FusedBatchNorm":
            gamma = node.input[1]
            beta = node.input[2]
            tmp_graph = tf.Graph()
            with tmp_graph.as_default():
                new_gamma_op = tf.constant(
                    1.0,
                    dtype=tf.float32,
                    shape=list(
                        d.size
                        for d in nodes[node_name(gamma)].attr['value'].tensor.tensor_shape.dim),
                    name=node.name + "/gamma_one").op.node_def
                node.input[1] = new_gamma_op.name
                result_nodes.append(new_gamma_op)
                new_beta_op = tf.constant(
                    0.0,
                    dtype=tf.float32,
                    shape=list(
                        d.size
                        for d in nodes[node_name(beta)].attr['value'].tensor.tensor_shape.dim),
                    name=node.name + "/beta_zero").op.node_def
                node.input[2] = new_beta_op.name
                result_nodes.append(new_beta_op)
            result_nodes.append(node)
            gamma_name = node.name + "/gamma"
            beta_name = node.name + "/beta"
            result_nodes.append(
                NodeDef(
                    name=gamma_name,
                    op='Mul',
                    input=[node.name, gamma],
                    attr={'T': node.attr['T']},
                ))
            result_nodes.append(
                NodeDef(
                    name=beta_name,
                    op='Add',
                    input=[node.name, beta],
                    attr={'T': node.attr['T']},
                ))
            for fix_node in graph.node:
                if any(node.name in inp for inp in fix_node.input):
                    for i, inp in enumerate(fix_node.input):
                        if inp == node.name:
                            fix_node.input[i] = beta_name
            fix_counter += 1
        else:
            result_nodes.append(node)
    return tf.GraphDef(node=result_nodes)


def create_optimized_graph(graph):
    from tensorflow.tools.graph_transforms import TransformGraph
    from tensorflow.python.tools import optimize_for_inference_lib

    inp_node = 'conv2d_1_input'
    out_node = 'activation_7/Softmax'
    graph_def = optimize_for_inference_lib.optimize_for_inference(
        graph,
        [inp_node],
        [out_node],
        tf.float32.as_datatype_enum,
    )
    graph_def = TransformGraph(graph_def, [inp_node], [out_node], ["sort_by_execution_order"])
    return graph_def


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        input_graph_def = graph.as_graph_def()
        input_graph_def = fix_batch_norm(input_graph_def)
        input_graph_def = create_optimized_graph(input_graph_def)
        freeze_var_names = list(
            set(v.name for v in input_graph_def.node).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.name for v in input_graph_def.node]
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = tf.graph_util.convert_variables_to_constants(session, input_graph_def,
                                                                    output_names, freeze_var_names)
    return frozen_graph


def fpr(y_true, y_pred):
    # Calculate how many background images were predicted false
    pred_class = tf.argmax(y_pred, 1)
    label = tf.argmax(y_true, 1)
    zeros = tf.zeros_like(label)
    ones = tf.ones_like(label)

    fp_op = tf.reduce_sum(
        tf.cast(
            tf.logical_and(
                tf.equal(label, zeros),
                tf.equal(pred_class, ones),
            ),
            "float",
        ),
    )
    amount_backgrounds = tf.reduce_sum(tf.cast(tf.equal(label, zeros), tf.int8))

    def calc_fpr():
        return fp_op / tf.cast(amount_backgrounds, tf.float32)

    fpr_op = tf.cond(tf.equal(amount_backgrounds, 0), lambda: 0.0, calc_fpr)

    K.get_session().run(tf.local_variables_initializer())
    return fpr_op


def tpr(y_true, y_pred):
    # Ball class is 1, ergo to find false positives need to find
    # all cases where 1 is predicted but label is 0

    # Calculate how many background images were predicted false
    pred_class = tf.argmax(y_pred, 1)
    label = tf.argmax(y_true, 1)
    zeros = tf.zeros_like(label)
    ones = tf.ones_like(label)

    tp_op = tf.reduce_sum(
        tf.cast(
            tf.logical_and(
                tf.equal(label, ones),
                tf.equal(pred_class, ones),
            ),
            "float",
        ),
    )
    amount_balls = tf.reduce_sum(tf.cast(tf.equal(label, ones), tf.int8))

    def calc_tpr():
        return tp_op / tf.cast(amount_balls, tf.float32)

    tpr_op = tf.cond(tf.equal(amount_balls, 0), lambda: 0.0, calc_tpr)

    K.get_session().run(tf.local_variables_initializer())
    return tpr_op


def global_average_pooling(x):
    return K.mean(x, axis=(1, 2))


def global_average_pooling_shape(input_shape):
    return (None, 2)


if __name__ == '__main__':
    model = Sequential()
    model.add(Conv2D(filters=5, kernel_size=(3, 3), input_shape=(15, 15, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=5, kernel_size=(3, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=5, kernel_size=(3, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=5, kernel_size=(3, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=5, kernel_size=(3, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Conv2D(filters=5, kernel_size=(3, 3)))
    #  model.add(BatchNormalization())
    model.add(Activation('relu'))
    model.add(Reshape((3 * 3 * 5, )))
    model.add(Dense(2))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy', fpr, tpr])

    batch_size = 32

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        rotation_range=30,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
    )

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    # this is a generator that will read pictures found in
    # subfolers of 'data/train', and indefinitely generate
    # batches of augmented image data
    train_generator = train_datagen.flow_from_directory(
        '/data/train',  # this is the target directory
        target_size=(15, 15),  # all images will be resized to 15x15
        batch_size=batch_size,
        class_mode='categorical')

    # this is a similar generator, for validation data
    validation_generator = test_datagen.flow_from_directory(
        '/data/val',
        target_size=(15, 15),
        batch_size=batch_size,
        class_mode='categorical',
    )

    class SaveModel(Callback):
        def on_epoch_end(self, epoch, logs=None):
            frozen_graph = freeze_session(
                K.get_session(),
                output_names=[out.op.name for out in self.model.outputs],
            )
            tf.train.write_graph(
                frozen_graph,
                os.path.dirname(os.path.realpath(__file__)),
                "model_epoch_{}.pb".format(epoch),
                as_text=False,
            )
            tf.train.write_graph(
                frozen_graph,
                os.path.dirname(os.path.realpath(__file__)),
                "model_epoch_{}.pbtxt".format(epoch),
            )

    class SaveLog(Callback):
        def on_epoch_end(self, epoch, logs=None):
            # Save metrics and weights
            logfile = os.path.dirname(os.path.realpath(__file__)) + "/metrics_log.json"
            if epoch <= 0:
                with open(logfile, 'w') as file:
                    json.dump({'epoch_{}'.format(epoch): logs}, file, indent=4)
            else:
                with open(logfile, 'r') as file:
                    content = json.load(file)

                with open(logfile, 'w') as file:
                    content.update({'epoch_{}'.format(epoch): logs})
                    json.dump(content, file, indent=4)

            # Save weights
            logfile = os.path.dirname(
                os.path.realpath(__file__)) + "/weights_epoch_{}.json".format(epoch)
            weight_dict = dict()
            for layer in self.model.layers:
                weights = layer.get_weights()
                if len(weights) > 0:
                    if len(weights) == 1:
                        weight_dict[layer.name] = weights[0].tolist()
                    elif len(weights) == 2:
                        weight_dict[layer.name] = {
                            'weights': weights[0].tolist(),
                            'biases': weights[1].tolist(),
                        }
                    else:
                        print("UNKNOWN WEIGHT LENGTH! Layer: {}".format(layer.name))

            with open(logfile, 'w') as file:
                json.dump(weight_dict, file, indent=4)

    model.fit_generator(
        train_generator,
        steps_per_epoch=500 // batch_size,
        epochs=20,
        validation_data=validation_generator,
        validation_steps=7044 // batch_size,
        callbacks=[SaveModel()],
        #  callbacks=[SaveModel(), SaveLog()],
        use_multiprocessing=True,
        workers=8,
    )
